2048
==============

This is my implementation of the game 2048, for now without any GUI, input and output is done via basic terminal commands.

***

### To do:
- [ ] clean up code
- [ ] port to windows
- [ ] add a GUI (GTK+, ncurses, in windows WPF)
- [ ] use as engine in Windows UWP app
- [ ] add more features
