#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
** struktura obsahujuca policka spolu s volnymi
*/
typedef struct{
  int velkost;
  int skore;
  int p_free; // pocet volnych policok
  int **ptr_free; // ukazatele na volne policka
  int *elements; // obsahuje hraciu plochu
}TABLE;

/*
** nahodne vyberie cislo a policko
*/
int random_p(TABLE *t){
  int number;

  if(t->p_free == 0){
    return 0;
  }

  /* initialize random seed */
  srand(time(NULL));
  /* geberate random number */
  number = rand() % t->p_free;

  *(t->ptr_free[number]) = 2;
  t->ptr_free[number] = t->ptr_free[t->p_free -1];
  t->p_free--;

  return 1;
}

/*
** inicializacia struktury TABLE
*/
void init_t(TABLE *t){
  int i, j;

  // inicializaci velkosti
  printf("Board size: ");
  scanf("%d", &t->velkost);

  // inicializacia skora
  t->skore = 0;
  t->p_free = t->velkost*t->velkost;

  // inicializacia plochy
  t->elements = (int*) malloc(sizeof(int)*t->velkost*t->velkost);
  for(i = 0; i<t->velkost; i++){
    for(j = 0; j<t->velkost; j++){
      t->elements[i*t->velkost+j] = 0;
    }
  }

  // inicializacia ukazatelov
  t->ptr_free = (int**) malloc(sizeof(int*)*t->velkost*t->velkost);
  for(i = 0; i<t->velkost; i++){
    for(j = 0; j<t->velkost; j++){
      t->ptr_free[i * t->velkost + j] = &(t->elements[i*t->velkost+j]);
    }
  }

  random_p(t);
  random_p(t);
}

void remove_from_ptrs(TABLE *t, int *ptr){
  int i = 0;

  while(t->ptr_free[i] != ptr){
    if(i == t->p_free){
      return;
    }
    i++;
  }
  t->ptr_free[i] = t->ptr_free[--t->p_free];
}

void add_to_ptrs(TABLE *t, int *ptr){
  *ptr = 0;
  t->ptr_free[t->p_free++] = ptr;
}

/* posunie policka */
void shift(TABLE *t, int ji, int jp, int pi, int o){ // pociatocne j, inkr j, pociatocne p, l
  int p; // pomocne policko
  int *x, *y;

  for(int i = 0; i < t->velkost; i++){ // prechadza po stlpcoch
    p = pi;
    for(int j = ji; -1 < j && j < t->velkost; j += jp){ // prechadza po riadkoch
    //  printf("j:%d, p:%d\n", j, p);
      if(o){
        x = t->elements + i*t->velkost + j;
        y = t->elements + i*t->velkost + p;
      }
      else{
        x = t->elements + j*t->velkost + i;
        y = t->elements + p*t->velkost + i;
      }

      if(*x != 0){ // cislo je nenulove
        // zlucenie
        if(*x == *y){
              //  printf("ZHODA -> j:%d, p:%d\n", j, p);
          *y *= 2;
          t->skore += *y;
          add_to_ptrs(t, x);
          p += jp;
          continue;
        }
        // posunutie
        if(*y == 0){
          *y = *x;
          add_to_ptrs(t, x); // prida prvok, z ktoreho presuvam
          remove_from_ptrs(t, y); // odstrani prvok, do ktoreho presuvam
          continue;
        }
        // inak posunie p a j ostava
        else{
          if(p + jp != j){
            p += jp;
            j += jp * (-1);
          }
        }
      }
    }
  }
}

/*
** vypis hracej plochy na terminal
*/
void print_t(TABLE *t){
  int i, j;

  printf("Skore: %d\n", t->skore);
  for(i = 0; i<t->velkost; i++){
    putchar('|');
    for(j = 0; j<t->velkost; j++){
      printf(" %d |", t->elements[i*t->velkost+j]);
    }
    putchar('\n');
  }
}

void exit_g(TABLE *t){
  free(t->ptr_free);
  free(t->elements);
}

int main(){
  TABLE t;
  char z;

  init_t(&t);

  print_t(&t);

  do{
    z = getchar();
    if(z == '\n') continue;


    switch(z){
      case 'a': // posun dolava
        shift(&t, 1, 1, 0, 1);
        if(random_p(&t) == 0){
          return 1;
        }
        break;
      case 'd': // posun doprava
        shift(&t, t.velkost-2, -1, t.velkost-1, 1);
        if(random_p(&t) == 0){
          return 1;
        }
        break;
      case 'w': // posun hore
        shift(&t, 1, 1, 0, 0);
        if(random_p(&t) == 0){
          return 1;
        }
        break;
      case 's': // posun dole
        shift(&t, t.velkost-2, -1, t.velkost-1, 0);
        if(random_p(&t) == 0){
          return 1;
        }
        break;
      case 'e': // new game
        printf("Initializing the game\n");
        init_t(&t);
        break;
      case 'q': // ukonci hru
        printf("Exitting game\n" );
        exit_g(&t);
        return 0;
      default: // neplatny posun
        printf("Invalid move\n" );
    }
    print_t(&t);

  }while(1);

  return 0;
}
